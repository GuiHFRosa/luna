using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
public class HomeController : ControllerBase
{

    [HttpGet("/")]
    public ActionResult Home()
    {
        return Ok("Api Foda!");
    }
}